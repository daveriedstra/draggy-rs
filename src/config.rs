use clap::{App, Arg, crate_version, crate_authors};

pub fn get_config() -> Config {
  let matches = App::new("draggy")
    .version(crate_version!())
    .author(crate_authors!())
    .about("3- or 4-finger drag for Linux.")
    // count
    .arg(Arg::with_name("count")
      .short('c')
      .long("count")
      .takes_value(true)
      .about("The number of fingers to enable drag for (3 or 4).")
    )
    // device path
    .arg(Arg::with_name("device_path")
      .short('d')
      .long("device_path")
      .takes_value(true)
      .about("Path of device to read from, eg, /dev/input/event14")
    )
    // grab
    .arg(Arg::with_name("grab")
      .short('g')
      .long("grab")
      .about("Exclusively handle input device during drag. Experimental. This would prevent default DE handling of the specified gestures, which is useful in cases where they  can\'t be configured, but this feature is still buggy: sometimes the drag gets stuck.")
    )
    .get_matches();

    let count: usize = matches.value_of_t("count").unwrap_or(3);
    if count != 3 && count != 4 {
      panic!("Couldn't initiate drag for touch count of {}; only 3 or 4 fingers are supported.", count);
    }

    let device_path = if let Some(path) = matches.value_of("device_path") {
      Some(String::from(path))
    } else {
      None
    };

    let grab = matches.is_present("grab");
    if grab {
      println!("Using experimental grab feature...");
    }

    Config {
      count,
      device_path,
      grab,
    }
}

pub struct Config {
  pub count: usize,
  pub device_path: Option<String>,
  pub grab: bool,
}
