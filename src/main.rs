mod gesture_device;
mod config;

use crate::gesture_device::{get_gesture_devices, get_gesture_device_at_path};
use crate::config::get_config;

fn main() -> Result<(), String> {
  let cfg = get_config();

  if let Some(ref path) = cfg.device_path {
    let mut dev = get_gesture_device_at_path(path)?;
    dev.handle_events(&cfg);
  } else {
    let mut devices = get_gesture_devices()?;
    for dev in devices.iter_mut() {
      println!("Got gesture device: {}", dev.original.name().unwrap());
      dev.handle_events(&cfg);
    }
  }

  Ok(())
}
