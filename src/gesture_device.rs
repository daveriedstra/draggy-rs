use std::fs::{File, read_dir};
use std::path::Path;

use evdev_rs::enums::{EventCode, EV_KEY, EV_ABS, EV_SYN};
use evdev_rs::{ReadFlag, ReadStatus, InputEvent};

use uinput::event::Event::{Absolute, Controller};
use uinput::event::Absolute::{Multi, Position, Digi as AbsDigi};
use uinput::event::absolute::Multi::{
  Slot,
  TrackingId,
  PositionX as MtPositionX,
  PositionY as MtPositionY,
  Pressure as MtPressure
};
use uinput::event::absolute::Position::{X, Y};
use uinput::event::absolute::Digi::Pressure as AbsPressure;

use uinput::event::Controller::{Digi, Mouse};
use uinput::event::controller::Digi::{Touch, Finger};
use uinput::event::controller::Mouse::{Left, Right};

use crate::config::Config;

pub fn get_gesture_devices() -> Result<Vec<GestureDevice>, String> {
  let devices: Vec<GestureDevice> = read_dir("/dev/input/")
    .map_err(|err| format!("{}", err))?
    .filter(|res| res.is_ok())
    .map(|dir| GestureDevice::new(&dir.unwrap().path()))
    .filter_map(|dev| dev.ok())
    .collect();

  if devices.len() == 0 {
    Err(String::from("No devices found"))
  } else {
    Ok(devices)
  }
}

pub fn get_gesture_device_at_path(path: &str) -> Result<GestureDevice, String> {
  let p = Path::new(path);
  GestureDevice::new(p)
}

enum Axis { X, Y }

/**
 * A GestureDevice is a reference to the physical input device (an evdev device)
 * and to the virtual uinput device which this program generates events for
 */
pub struct GestureDevice {
  pub original: evdev_rs::Device,
  pub surrogate: uinput::Device,
  is_dragging: bool,
}

impl GestureDevice {

  /**
   * Try to make a new GestureDevice from a /dev/input/event# path
   */
  pub fn new(path: &Path) -> Result<GestureDevice, String> {
    let file = File::open(path).unwrap();
    match evdev_rs::Device::new_from_fd(file) {
      Err(e) => return Err(format!("{:?}", e)),
      Ok(dev) => {
        if GestureDevice::is_gesture_device(&dev) {
          GestureDevice::make_gesture_device(dev)
        } else {
          Err(format!("{} is not a valid gesture device!", path.display()))
        }
      }
    }
  }

  /**
   * Construct a GestureDevice from a known-good evdev input device
   */
  fn make_gesture_device(dev: evdev_rs::Device) -> Result<GestureDevice, String> {
    match GestureDevice::make_surrogate(&dev) {
      Err(e) => Err(format!("{:?}: {}", e, e)),
      Ok(surrogate) => Ok(GestureDevice {
        surrogate: surrogate,
        original: dev,
        is_dragging: false,
      })
    }
  }

  /**
   * Determines whether an evdev device is a touchpad with gesture capabilities
   */
  fn is_gesture_device(dev: &evdev_rs::Device) -> bool {
    !dev.name().unwrap().contains("draggy") &&
      dev.has(&EventCode::EV_KEY(EV_KEY::BTN_TOOL_TRIPLETAP)) &&
      dev.has(&EventCode::EV_KEY(EV_KEY::BTN_TOOL_FINGER)) &&
      dev.has(&EventCode::EV_KEY(EV_KEY::BTN_LEFT)) &&
      dev.has(&EventCode::EV_KEY(EV_KEY::BTN_TOUCH))
  }

  /**
   * Create a "surrogate" virtual UInput device to match the input evdev device
   */
  fn make_surrogate(original: &evdev_rs::Device) ->
    Result<uinput::Device, uinput::Error> {
    let orig_name = original.name().unwrap();
    let abs_y_opts = original.abs_info(&EventCode::EV_ABS(EV_ABS::ABS_Y)).unwrap();
    let abs_x_opts = original.abs_info(&EventCode::EV_ABS(EV_ABS::ABS_X)).unwrap();

    let builder = uinput::default()?
      .name(format!("{}-draggy-rs", orig_name))?
      .vendor(original.vendor_id())
      .product(original.product_id())
      .version(original.version())
      .bus(original.bustype())
      .event(Controller(Digi(Touch)))?
      .event(Controller(Digi(Finger)))?
      .event(Controller(Mouse(Left)))?
      .event(Controller(Mouse(Right)))?
      .event(Absolute(Multi(TrackingId)))?
      .event(Absolute(Multi(MtPressure)))?
        .min(0).max(155)
      .event(Absolute(AbsDigi(AbsPressure)))?
        .min(0).max(155)
      .event(Absolute(Position(X)))?
        .min(abs_x_opts.minimum)
        .max(abs_x_opts.maximum)
        .fuzz(abs_x_opts.fuzz)
        .flat(abs_x_opts.flat)
      .event(Absolute(Position(Y)))?
        .min(abs_y_opts.minimum)
        .max(abs_y_opts.maximum)
        .fuzz(abs_y_opts.fuzz)
        .flat(abs_y_opts.flat)
      .event(Absolute(Multi(MtPositionX)))?
        .min(abs_x_opts.minimum)
        .max(abs_x_opts.maximum)
        .fuzz(abs_x_opts.fuzz)
        .flat(abs_x_opts.flat)
      .event(Absolute(Multi(MtPositionY)))?
        .min(abs_y_opts.minimum)
        .max(abs_y_opts.maximum)
        .fuzz(abs_y_opts.fuzz)
        .flat(abs_y_opts.flat)
      .event(Absolute(Multi(Slot)))?
        .min(0)
        .max(10)
      ;

    Ok(builder.create()?)
  }

  /**
   * Main event loop per device
   */
  pub fn handle_events(&mut self, cfg: &Config) {
    loop {
      // this could also be an Err I guess
      if let Ok((status, ev)) = self.original.next_event(ReadFlag::NORMAL | ReadFlag::BLOCKING) {
        match status {
          ReadStatus::Sync => {},
          ReadStatus::Success => {
            if let Err(e) = self.handle_event(ev, cfg) {
              println!("{}", e);
            }
          }
        }
      }
    }
  }

  /**
   * Single event handler
   */
  fn handle_event(&mut self, ev: InputEvent, cfg: &Config) -> Result<(), uinput::Error> {
    match ev.event_code {
      EventCode::EV_KEY(EV_KEY::BTN_TOOL_TRIPLETAP) => {
        if cfg.count == 3 {
          self.send_click_frame(ev.value != 0, 1997, cfg.grab)?;
        }
      },
      EventCode::EV_KEY(EV_KEY::BTN_TOOL_QUADTAP) => {
        if cfg.count == 4 {
          self.send_click_frame(ev.value != 0, 1997, cfg.grab)?;
        }
      },
      EventCode::EV_SYN(EV_SYN::SYN_REPORT) => {
        if self.is_dragging {
          self.surrogate.synchronize()?;
        }
      },
      EventCode::EV_ABS(EV_ABS::ABS_X) => {
        if self.is_dragging {
          self.move_x(ev.value, 1997)?;
        }
      },
      EventCode::EV_ABS(EV_ABS::ABS_Y) => {
        if self.is_dragging {
          self.move_y(ev.value, 1997)?;
        }
      }
      // using these MT events would require tracking slots ourself
      // EventCode::EV_ABS(EV_ABS::ABS_MT_POSITION_X) => {
      //   if self.is_dragging {
      //     self.move_x(ev.value, 1997).?;
      //   }
      // },
      // EventCode::EV_ABS(EV_ABS::ABS_MT_POSITION_Y) => {
      //   if self.is_dragging {
      //     self.move_y(ev.value, 1997).?;
      //   }
      // }
      _ => {}
    };

    Ok(())
  }

  /**
   * Sends the first or last frame of the three-finger drag sequence
   */
  fn send_click_frame(&mut self, start: bool, tracking_id: i32, grab: bool)
    -> Result<(), uinput::Error> {

    let value = if start { 1 } else { 0 };
    self.is_dragging = start;

    if grab {
      if start {
       if let Err(msg) = self.original.grab(evdev_rs::GrabMode::Grab) {
         println!("Could not grab device: {}", msg);
       }
      } else {
        if let Err(msg) = self.original.grab(evdev_rs::GrabMode::Ungrab) {
         println!("Could not ungrab device: {}", msg);
        }
      }
    }

    self.surrogate.send(Absolute(Multi(Slot)), 0)?;
    self.surrogate.send(Absolute(Multi(TrackingId)), tracking_id)?;
    self.surrogate.send(Controller(Digi(Touch)), value)?;
    self.surrogate.send(Controller(Digi(Finger)), value)?;
    self.surrogate.send(Absolute(Multi(MtPressure)), value * 55)?;
    self.surrogate.send(Absolute(AbsDigi(AbsPressure)), value * 55)?;

    self.surrogate.send(Controller(Mouse(Left)), value)?;

    self.surrogate.synchronize()?;

    Ok(())
  }

  /**
   * Sends an X position frame
   */
  fn move_x(&mut self, value: i32, tracking_id: i32) -> Result<(), uinput::Error> {
    self.send_pos_frame(value, tracking_id, Axis::X)
  }

  /**
   * Sends an X position frame
   */
  fn move_y(&mut self, value: i32, tracking_id: i32) -> Result<(), uinput::Error> {
    self.send_pos_frame(value, tracking_id, Axis::Y)
  }

  /**
   * Sends an arbitrary position frame
   */
  fn send_pos_frame(&mut self, value: i32, tracking_id: i32, axis: Axis) -> Result<(), uinput::Error> {
    self.surrogate.send(Absolute(Multi(Slot)), 0)?;
    self.surrogate.send(Absolute(Multi(TrackingId)), tracking_id)?;
    match axis {
      Axis::X => {
        self.surrogate.send(Absolute(Multi(MtPositionX)), value)?;
        self.surrogate.send(Absolute(Position(X)), value)?;
      },
      Axis::Y => {
        self.surrogate.send(Absolute(Multi(MtPositionY)), value)?;
        self.surrogate.send(Absolute(Position(Y)), value)?;
      }
    };
    Ok(())
  }
}
